﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class PlayerSetup : NetworkBehaviour
{

    [SyncVar(hook = "UpdateColor")] //zmienna domyslnie przekazywana do metody jako argument
    //chcemy aby kolory oraz nazwy byly zgodne podczas polaczenia przez siec
    public Color m_playerColor; //kolor
    public string m_basename = "PLAYER"; //każdy gracz będzie posiadał nazwę Player oraz odpowiadający mu numer

    [SyncVar(hook = "UpdateName")]
    public int m_playerNum = 1; 
    public Text m_playerNameText;


    void Start()
    {
        if (!isLocalPlayer)
        {
            UpdateName(m_playerNum);
            UpdateColor(m_playerColor);
        }
    }

    public override void OnStartClient() //An override method provides a new implementation of a member that is inherited from a base class.
    { //wszystko wewnatrz, bedzie uruchomione, jezeli biezacy gracz jest localplayerem
        base.OnStartClient();
        if (m_playerNameText != null)
        {
            m_playerNameText.enabled = false;
        }
    }

    void UpdateColor(Color pColor)
    {
        MeshRenderer[] meshes = GetComponentsInChildren<MeshRenderer>(); //inspector 
        foreach (MeshRenderer r in meshes)
        {
            r.material.color = pColor;
        }
    }

    void UpdateName(int pNum)
    {
        if (m_playerNameText != null) //
            //Jeśli tekst nazwy gracza nie jest równy null, włącz tekst, a następnie ustaw tekst równy podstawie
        {
            m_playerNameText.enabled = true; //włącz tekst, a następnie ustaw tekst równy podstawie
            m_playerNameText.text = m_basename + pNum.ToString(); //potem dodaj numer odtwarzacza przekonwertowany na ciąg.
        }
    }

    public override void OnStartLocalPlayer()// metoda, którą otrzymujemy z networkbehaviour i która działa wcześniej niz startlocalplayer
    {
        base.OnStartLocalPlayer();
        CmdSetupPlayer(); //gdy kazdy klient sie polaczy
    }


    //wiemy, że nasz localplayer zostal ustawiony wlasciwie na kliencie
    //potrzebujemy czegos, co ustali gdy polaczy sie on z sesja gry -> zaktualizuje sie na serwerze
    //Za każdym razem, gdy wykonuje sie funkcję w sieci, prawdopodobnie trzeba wykonać remote action 
    [Command] //command to remote action wywolywana na kliencie, ale uruchamiana na serwerze
    void CmdSetupPlayer()
    {
        GameManager.Instance.AddPlayer(this); //odwolanie sie do addplayera
        GameManager.Instance.m_playerCount++; //inkrementacja m_playerCount, chcemy to, gdy stanie sie to na serwerze
    }


}
