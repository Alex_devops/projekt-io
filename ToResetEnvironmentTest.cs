﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SetUp]
public void ResetScene()
{
    //Podczas uruchamiania testów, które tworzą i niszczą GameObjects, dobrą praktyką jest resetowanie środowiska,
    //aby zapobiec niezamierzonym skutkom ubocznym poprzednich testów.
   
    EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);

}