using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : NetworkBehaviour {

	Rigidbody m_rigidbody; 
	
	/*
	  Rigidbodies enable your GameObjects
      to act under the control of physics. 
      The Rigidbody can receive forces and torque to make your objects move in a realistic way. 
      Any GameObject must contain a Rigidbody to be influenced by gravity, act under added forces 
      via scripting, or interact with other objects through the NVIDIA PhysX physics engine
	 

	*/

	public Transform m_chassis; // Transform - Position, rotation and scale of an object.

	public Transform m_turret;

	public float m_moveSpeed = 100f; //predkosc poruszania sie graczy

	public float m_chassisRotateSpeed = 1f;  //predkosc obrotu podwozia
	
	public float m_turretRotateSpeed = 3f;  // predkosc obrotu wiezy
	// public Vector3 m_turretDirection;
  //  public Vector3 m_chassisDirection;

   // Use this for initialization
	void Start () 
	{
		m_rigidbody = GetComponent<Rigidbody>();
	}

	public void MovePlayer(Vector3 dir) //metoda, ktora sprawi, że bedziemy poruszac sie w strone, w ktora wskazuje podwozie
	{
		Vector3 moveDirection = dir * m_moveSpeed * Time.deltaTime;
		m_rigidbody.velocity = moveDirection;

	}

	public void FaceDirection(Transform xform, Vector3 dir, float rotSpeed) // 1. transform we want to rotate, 2. Wektor reprezentujacy kierunek w ktorym chcemy, żeby sie obrociło nasze "Transform" 
																			// 3. predkosc do zwracania się w nowym kierunku //
	{
		if (dir != Vector3.zero && xform != null)
		{
			Quaternion desiredRot = Quaternion.LookRotation(dir); //konwertowanie vectora3 na quaternion
			
			/*
			Quaternions are used to represent rotations.
			*/
			
			xform.rotation = Quaternion.Slerp(xform.rotation, desiredRot, rotSpeed * Time.deltaTime); // Slerp : Spherically interpolates between two vectors. public static Vector3 Slerp(Vector3 a, Vector3 b, float t);
			//Czyli przemiana jednego kata w drugi. Bierzemy transform i obracamy go pod innym katem 
		}
	}

	public void RotateChassis(Vector3 dir)
	{
		FaceDirection(m_chassis, dir, m_chassisRotateSpeed);
	}

	public void RotateTurret(Vector3 dir)
	{
		FaceDirection(m_turret, dir, m_turretRotateSpeed);
	}







// Update is called once per frame
	//void Update () {
		
	//}








}
