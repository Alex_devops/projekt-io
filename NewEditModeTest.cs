﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Test]

public void BasicTest()
{
    bool isActive = false;
    Assert.AreEqual(false, isActive);
}

//To jeden z najprostszych testów do stworzenia w Unity.
//Nie ma wiedzy o świecie poza sobą i nie wymaga zaawansowanych obliczeń, aby określić pozytywny lub negatywny wynik.


//[Test] u góry oznacza, że jest to test w trybie edycji.
//Ciało testu jest po prostu twierdzeniem potwierdzającym, że zmienna isActive jest tym, czego się spodziewasz - fałszem.

