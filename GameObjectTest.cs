﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Test]
public void CatchingErrors()
{

    GameObject gameObject = new GameObject("test");

    Assert.Throws<MissingComponentException>(
        () => gameObject.GetComponent<Rigidbody>().velocity = Vector3.one);

}

//Ten test ma podobną strukturę do pierwszego testu, ponieważ jest w większości samowystarczalny.
//Jednak w tym przykładzie tworzony jest nowy obiekt GameObject i dodawany do aktywnej sceny. 
  //  Test próbuje ustalić prędkość przyłączonego ciała sztywnego.
//Ale ten GameObject został właśnie stworzony, więc nie ma on sztywnego nadwozia i powinien zgłosić błąd.